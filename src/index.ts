/* eslint-disable */
import './index.css'; // 引入样式
import { openDialog } from './components/openDialog';
import Switch from './components/Switch.vue';
import Button from './components/Button.vue';
import Dialog from './components/Dialog.vue';
import Tab from './components/Tab.vue';
import Tabs from './components/Tabs.vue';

export { openDialog, Switch, Button, Dialog, Tab, Tabs };
